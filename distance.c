#include <stdio.h> 
#include <math.h>

int main() 
{
	float x1, y1, x2, y2, gdistance;
	printf("Input x1: ");
	scanf("%d", &x1);
	printf("Input y1: ");
	scanf("%d", &y1);
    printf("Input x2: ");
	scanf("%d", &x2);
	printf("Input y2: ");
	scanf("%d", &y2);
	gdistance = ((x2-x1)*(x2-x1))+((y2-y1)*(y2-y1));
	printf("Distance between the said points: %.4f", sqrt(gdistance));
	printf("\n");
	return 0;
}